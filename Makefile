backup:
	tar cvfz /tmp/orbit-server.tgz \
                --exclude=node_modules \
                --exclude=node_modules.old \
                --exclude="*.mdb" \
                --exclude="dist" \
                --exclude="build" \
                --exclude=".cache" \
                --exclude="*.log" \
                --exclude="*.zip" \
                --exclude=".random_errors" \
                --exclude=".pytest*" \
                --exclude="*__pycache__*" \
                --exclude=".attic" \
                --exclude=".tox" \
                --exclude=".venv"  \
                --exclude=".crap"  \
                --exclude="htmlcov"  \
                --exclude=".git" \
                --exclude=".thumbnail_cache" \
                --exclude=".parts_cache" \
                .
