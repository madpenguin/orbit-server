from socketio import AsyncNamespace
from orbit.orbit_auth import OrbitAuth
from orbit.orbit_database import OrbitDatabase
from orbit.schema.Sessions import SessionsCollection
from orbit.orbit_shared import world
from orbit.orbit_decorators import navGuard
from loguru import logger as log


class OrbitAPI (AsyncNamespace):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._database = None
        self._nql = None

    def open (self, nql=None, auditing=True):
        self._nql = nql
        self._auditing = auditing
        self._database = OrbitDatabase().open(nql=nql)
        SessionsCollection().empty()
        if nql:
            self._nql.open(self.emit)
    
    async def on_connect(self, sid, environ):
        await self.save_session(sid, {
            'sid': sid,
            'address': environ['aiohttp.request'].transport.get_extra_info('peername')[0]
        })
        if self._nql:
            self._nql.connect(sid)

    async def on_disconnect(self, sid):
        SessionsCollection(sid).disconnect()
        if self._nql:
            self._nql.disconnect(sid)

    async def on_auth_hello(self, sid, auth):
        return await OrbitAuth(sid).hello(auth)

    async def on_auth_validate(self, sid, auth):
        return await OrbitAuth(sid).validate(auth)

    async def on_auth_confirm(self, sid, auth):
        return await OrbitAuth(sid).confirm(auth)

    @navGuard
    async def on_call_nql(self, sid, params):
        return self._nql.call(sid, params)

    @navGuard
    async def on_drop_nql(self, sid, params):
        return self._nql.drop(sid, params)

    @navGuard
    async def on_dump_nql(self, sid):
        return self._nql.dump ()
