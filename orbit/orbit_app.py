from aiohttp import web
from aiohttp.web_runner import GracefulExit
from asyncio import sleep
from multiprocessing import freeze_support, set_start_method
from loguru import logger as log
from socketio import AsyncServer
from orbit.orbit_logger import OrbitLogger
from orbit.orbit_make_ssl import OrbitMakeSSL
from orbit.orbit_nql import NQL
from orbit.orbit_shared import world
from ssl import create_default_context, Purpose


class OrbitAPP:
    
    def __init__ (self, name, config, api, router):
        self._name = name
        self._config_cls = config
        self._api_cls = api
        self._router_cls = router

    async def startup (self, app=None):
        world.api.open (NQL())

    async def shutdown(self, app=None):
        await sleep(1)
        raise GracefulExit()

    def run (self):
        # set_start_method('spawn')
        # freeze_support()

        world.conf = self._config_cls(self._name).open()
        server_params = {
            'async_mode': 'aiohttp',
            'async_handlers': True,
            'engineio_logger': False,
            'cors_allowed_origins': '*'
        }
        if world.conf.sio_debug:
            server_params['logger'] = OrbitLogger()

        world.api = self._api_cls(namespace=world.conf.namespace)
        app = self._router_cls().application()
        app.on_startup.append(self.startup)
        app.on_shutdown.append(self.shutdown)

        world.sio = AsyncServer(**server_params)
        world.sio.attach(app, socketio_path=world.conf.namespace)
        world.sio.register_namespace(world.api)

        try:
            if world.conf.secure:
                ssl = OrbitMakeSSL().open()        
                ssl_context = create_default_context(Purpose.CLIENT_AUTH)
                ssl_context.load_cert_chain(ssl.crt, ssl.key)
                web.run_app(
                    app,
                    handle_signals=False,
                    host=world.conf.host,
                    port=world.conf.port,
                    ssl_context=ssl_context)            
            else:
                web.run_app(
                    app,
                    host="localhost",
                    port=world.conf.local_port)
        except Exception as e:
            log.exception(e)
        finally:
            print()
            log.info('Shutdown')
            raise GracefulExit()
