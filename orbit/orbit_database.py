from pathlib import Path
from pynndb import Manager
from orbit.orbit_shared import world
from orbit.schema.Users import UsersCollection
from orbit.schema.Sessions import SessionsCollection
from loguru import logger as log


class OrbitDatabase:

    PATH = 'default'
    FILE = None
    MAX_DATABASE_SIZE = 64
    COLLECTIONS = []
    CONFIG = {
        'map_size': 1024 * 1024 * 1024 * 64,
        'reindex': False,
        'auditing': True,
        'writemap': True
    }

    @property
    def real_path (self):
        return self._floc

    def __init__ (self, collections=None):
        self._path = self.PATH
        self._file = self.FILE
        self._collections = [UsersCollection, SessionsCollection] + self.COLLECTIONS + (collections or [])
        self._config = self.CONFIG
        self._floc = None
       
    def open(self, nql=None):
        if self._path == 'default':
            path = world.conf.database
        elif path and self._file:
            path = Path(self._path) / self._file
        else:
            raise Exception(f'bad database specification: {self._path} / {self._file}')
        if not path.exists():
            path.mkdir(parents=True, exist_ok=True)
        self._manager = Manager()
        self._database = self._manager.database('db', path.as_posix(), config=self._config)

        for collection in self._collections:
            collection().open(self, nql)

        self._floc = path.as_posix()
        return self
    
    def close (self):
        self._manager.close()

