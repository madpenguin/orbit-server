class Global:

    _sio = None
    _api = None
    _nql = None
    _conf = None
    _args = None
    _sessions = {}

    @property
    def sio (self):
        return self._sio

    @property
    def api (self):
        return self._api

    @property
    def nql (self):
        return self._nql

    @property
    def conf (self):
        return self._conf

    @property
    def args (self):
        return self._args

    @property
    def sessions (self):
        return self._sessions

    @api.setter
    def api (self, value):
        self._api = value

    @nql.setter
    def nql (self, value):
        self._nql = value

    @sio.setter
    def sio (self, value):
        self._sio = value

    @conf.setter
    def conf (self, value):
        self._conf = value

    @args.setter
    def args (self, value):
        self._args = value

    @sessions.setter
    def sessions (self, value):
        self._sessions = value

world = Global()
