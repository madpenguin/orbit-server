from orbit.orbit_orm import BaseTable, BaseCollection
from pynndb import SerialiserType, Doc
from datetime import datetime
from loguru import logger as log


class SessionsTable (BaseTable):

    norm_table_name = 'sessions'
    norm_auditing = True
    norm_codec = SerialiserType.UJSON
    norm_ensure = [
        {'index_name': 'by_when'   , 'duplicates': True , 'func': '{when:14.6f}'},
        {'index_name': 'by_sid'    , 'duplicates': False, 'func': '{sid}'},
    ]

    def from_sid (self, sid, transaction=None):
        self.set(self.norm_tb.seek_one('by_sid', Doc({'sid': sid}), txn=transaction))
        return self

    @property
    def when (self):
        return datetime.utcfromtimestamp(self._when).strftime('%Y-%m-%d %H:%M:%S')


class SessionsCollection (BaseCollection):

    table_class = SessionsTable
    table_strip = ['address', 'sid', 'user_id']

    def disconnect (self):
        log.error(f"Disconnect: {self._sid}")
        session = SessionsTable().from_sid(self._sid)
        if session.isValid:
            session.delete()

