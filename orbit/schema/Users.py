from orbit.orbit_orm import BaseTable, BaseCollection
from pynndb import SerialiserType, Doc
from loguru import logger as log


class UsersTable (BaseTable):

    norm_table_name = 'users'
    norm_auditing = True
    norm_codec = SerialiserType.UJSON


class UsersCollection (BaseCollection):

    table_class = UsersTable
    table_strip = ['user_id']

